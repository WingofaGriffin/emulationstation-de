# slate-DE for EmulationStation Desktop Edition

slate-DE is an official theme set for ES-DE that supports all systems as well as the latest application features.

The following options are included:

2 variants:

- Textlist with videos
- Textlist without videos

2 color schemes:

- Dark mode
- Light mode

2 aspect ratios:

- 16:9
- 4:3

There is no need for a dedicated 16:10 aspect ratio as the 16:9 layout scales really well.

# Credits

The theme is based on [recalbox-multi](https://gitlab.com/recalbox/recalbox-themes) by the Recalbox community.
