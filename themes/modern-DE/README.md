# modern-DE for EmulationStation Desktop Edition

modern-DE is an official theme set for ES-DE that supports all systems as well as the latest application features.

The following options are included:

2 variants:

- Textlist with videos
- Textlist without videos

2 color schemes:

- Dark mode
- Light mode

4 aspect ratios:

- 16:9
- 16:10
- 4:3
- 21:9

# Credits

The theme is based on [es-theme-switch](https://github.com/lilbud/es-theme-switch) by lilbud.

# Pictures

![](https://gitlab.com/es-de/emulationstation-de/uploads/8859cd1f6ebf5653b6eb12b4f5171ecc/image.png)
![](https://gitlab.com/es-de/emulationstation-de/uploads/920a19cf845cb65f98db4cee5158a26f/image.png)
![](https://gitlab.com/es-de/emulationstation-de/uploads/f3d7ac00964e92252bea000043c1cab5/image.png)
![](https://gitlab.com/es-de/emulationstation-de/uploads/7f317ad1dff31e0c2dd21e4d1930613d/image.png)
![](https://gitlab.com/es-de/emulationstation-de/uploads/6a87a0d94898cfe060b028e29f148147/image.png)
![](https://gitlab.com/es-de/emulationstation-de/uploads/6862b35b9142888f983d86fea65d5411/image.png)
![](https://gitlab.com/es-de/emulationstation-de/uploads/36994f94784ff12c0ea48520544ed4fb/image.png)
